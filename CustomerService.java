
@Service
public class CustomerService {
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void doSth() {
        
        doSthElse(); 
        
    }
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void doSthElse() {
        //rest of implementation
    }
}